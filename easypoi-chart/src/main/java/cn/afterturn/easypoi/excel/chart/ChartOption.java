package cn.afterturn.easypoi.excel.chart;

public class ChartOption {

    private Option option;

    public static ChartOption newChart() {
        ChartOption option = new ChartOption();
        option.option = new Option();
        return option;
    }

    public ChartOption setBackgroundColor(String backgroundColor) {
        option.setBackgroundColor(backgroundColor);
        return this;
    }

    public ChartOption setColor(String color) {
        option.setColor(color);
        return this;
    }

    public ChartOption setTimeline(Object timeline) {
        option.setTimeline(timeline);
        return this;
    }

    public ChartOption setTitle(Object title) {
        option.setTitle(title);
        return this;
    }

    public ChartOption setLegend(Object legend) {
        option.setLegend(legend);
        return this;
    }

    public ChartOption setDataRange(Object dataRange) {
        option.setDataRange(dataRange);
        return this;
    }

    public ChartOption setDataZoom(Object dataZoom) {
        option.setDataZoom(dataZoom);
        return this;
    }

    public ChartOption setGrid(Object grid) {
        option.setGrid(grid);
        return this;
    }

    public ChartOption setXAxis(Object xAxis) {
        option.setXAxis(xAxis);
        return this;
    }

    public ChartOption setYAxis(Object yAxis) {
        option.setYAxis(yAxis);
        return this;
    }

    public ChartOption setSeries(Object series) {
        option.setSeries(series);
        return this;
    }

    public Option getOption() {
        return option;
    }

}
