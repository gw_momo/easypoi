package cn.afterturn.easypoi.excel.chart.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author liuzh
 */
@Data
public class Legend {
    /**
     * 图例内容数组，数组项通常为{string}，每一项代表一个系列的name。
     */
    private List data = new ArrayList<>();

    public Legend() {
    }

    public Legend(List data) {
        this.data = data;
    }


    /**
     * 添加图例属性
     *
     * @param values
     * @return
     */
    public Legend data(Object... values) {
        if (values == null || values.length == 0) {
            return this;
        }
        this.data.addAll(Arrays.asList(values));
        return this;
    }
}
